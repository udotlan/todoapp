﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using todo_aspnetcoremvc_ui.Infrastructure;
using todo_domain_entities.Interfaces;
using todo_domain_entities.Models;

namespace todo_aspnetcoremvc_ui.Components
{
    public class NavigationMenuViewComponent:ViewComponent
    {
        private readonly ITodoListRepository _repo;
        private readonly IUserProvider _userProvider;
        public NavigationMenuViewComponent(ITodoListRepository repo, IUserProvider userProvider)
        {
            _repo = repo;
            _userProvider = userProvider;
        }
        public IViewComponentResult Invoke()
        {
            var userID = _userProvider.GetUserId();
            var category = _repo.TodoLists.Where(x => x.ApplicationUserId == userID).Select(x => x.Category).Distinct().OrderBy(x => x).ToList();
            ViewBag.Categories = category;
            ViewBag.SelectedCategory = RouteData?.Values["category"];
            ViewBag.ShowHiddenLists = Request.Query["showHiddenLists"];
            ViewBag.HideCompletedLists = Request.Query["hideCompletedLists"];
            return View(new TodoList());
        }
    }
}
