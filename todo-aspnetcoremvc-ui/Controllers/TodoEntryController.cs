﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using todo_aspnetcoremvc_ui.Infrastructure;
using todo_aspnetcoremvc_ui.Models;
using todo_aspnetcoremvc_ui.Models.ViewModels;
using todo_domain_entities.Interfaces;
using todo_domain_entities.Models;

namespace todo_aspnetcoremvc_ui.Controllers
{
    [Authorize]
    public class TodoEntryController : Controller
    {
        private readonly ITodoEntryRepository _entryRepo;
        private readonly ITodoListRepository _listRepo;
        private readonly IMapper _mapper;
        private readonly IUserProvider _userProvider;
        public TodoEntryController(ITodoEntryRepository repo, ITodoListRepository listRepo, IMapper mapper, IUserProvider userProvider = null)
        {
            _entryRepo = repo;
            _listRepo = listRepo;
            _mapper = mapper;
            _userProvider = userProvider;
        }

        public async Task<IActionResult> Index(ItemIndexViewModel model)
        {
            var todoList = _listRepo.TodoLists.FirstOrDefault(x => x.ID == model.Id);
            ItemIndexViewViewModel viewModel = new ItemIndexViewViewModel
            {
                TodoList = todoList,
                ShowDueTodayItems = model.ShowDueTodayItems,
                ShowOverDueItems = model.ShowOverDueItems,
                CurrentSort = model.SortOrder,
                DescriptionSort = string.IsNullOrEmpty(model.SortOrder) ? "desc_desc" : "",
                CreationDateSort = model.SortOrder == "cds" ? "cds_desc" : "cds",
                DueDateSort = model.SortOrder == "ds" ? "ds_desc" : "ds",
                PrioritySort = model.SortOrder == "ps" ? "ps_desc" : "ps",
                StatusSort = model.SortOrder == "ss" ? "ss_desc" : "ss",
                TodoListSort = model.SortOrder == "ts" ? "ts_desc" : "ts",
                BackUrl = model.BackUrl,
                Category = model.Category,
                ID = model.Id
            };

            if (model.Search != null)
            {
                model.PageNumber = 1;
            }
            else
            {
                model.Search = model.CurrentFilter;
            }

            viewModel.CurrentFilter = model.Search;

            var list = _entryRepo.TodoEntries.Include(x => x.TodoList)
                .Where(x => model.Category == null || x.TodoList.Category == model.Category);

            if (model.Id == null)
            {
                list = list.Where(x => x.TodoList.ApplicationUserId == _userProvider.GetUserId());
            }
            else
            {
                list = list.Where(x => x.TodoListID == model.Id);
            }

            if (model.ShowDueTodayItems)
            {
                list = list.Where(x => x.DueDate.Date == DateTime.Now.Date);
            }

            if (model.ShowOverDueItems)
            {
                list = list.Where(x => x.DueDate < DateTime.Now && x.EntryStatus != Status.Completed);
            }

            if (!string.IsNullOrEmpty(model.Search))
            {
                list = list.Where(x => x.Description.Contains(model.Search));
            }

            list = model.SortOrder switch
            {
                "desc_desc" => list.OrderByDescending(x => x.Description),
                "cds" => list.OrderBy(x => x.CreationDate),
                "cds_desc" => list.OrderByDescending(x => x.CreationDate),
                "ds" => list.OrderBy(x => x.DueDate),
                "ds_desc" => list.OrderByDescending(x => x.DueDate),
                "ps" => list.OrderBy(x => x.EntryPriority),
                "ps_desc" => list.OrderByDescending(x => x.EntryPriority),
                "ss" => list.OrderBy(x => x.EntryStatus),
                "ss_desc" => list.OrderByDescending(x => x.EntryStatus),
                "ts" => list.OrderBy(x => x.TodoList.Title),
                "ts_desc" => list.OrderByDescending(x => x.TodoList.Title),
                _ => list.OrderBy(x => x.Description),
            };

            int pageSize = 5;
            viewModel.List = await PaginatedList<TodoEntry>.CreateAsync(list.AsNoTracking(), model.PageNumber ?? 1, pageSize);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(ItemViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("_FormPartialView", viewModel);
            }

            var entry = _mapper.Map<TodoEntry>(viewModel);
            bool successfull = await _entryRepo.CreateTodoEntryAsync(entry);
            
            if (!successfull)
            {
                return BadRequest("Could not create item");
            }

            if (!string.IsNullOrEmpty(viewModel.ReturnUrl))
            {
                return Redirect(viewModel.ReturnUrl);
            }

            return RedirectToAction("Index");
        }

        public IActionResult Update(int? id, string returnUrl)
        {
            if (id != null)
            {
                var todoEntry = _entryRepo.TodoEntries.FirstOrDefault(x => x.ID == id);
                if (todoEntry != null)
                {
                    ItemViewModel viewModel = _mapper.Map<ItemViewModel>(todoEntry);
                    viewModel.ReturnUrl = returnUrl;
                    return View(viewModel);
                }
            }

            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(ItemViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var todoEntry = _entryRepo.TodoEntries.FirstOrDefault(x => x.ID == viewModel.ID);
            todoEntry = _mapper.Map(viewModel, todoEntry);
            var successfull = await _entryRepo.UpdateTodoEntryAsync(todoEntry);
            if (!successfull)
            {
                return BadRequest("Could not change item");
            }

            if (!string.IsNullOrEmpty(viewModel.ReturnUrl))
            {
                return Redirect(viewModel.ReturnUrl);
            }

            return RedirectToAction("Index"); 
        }

        public async Task<IActionResult> Delete(int? id, string returnUrl, int count)
        {
            if (id != null)
            {
                var entry = _entryRepo.TodoEntries.FirstOrDefault(x => x.ID == id);
                if (entry != null)
                {
                    var successfull = await _entryRepo.DeleteTodoEntryAsync(id.Value);
                    if (!successfull)
                    {
                        return BadRequest("Could not remove item");
                    }

                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        if (count > 1)
                        {
                            return Redirect(returnUrl);
                        }

                        int index = returnUrl.IndexOf('?');
                        string queryString = returnUrl[(index + 1)..];
                        var query = HttpUtility.ParseQueryString(queryString);
                        query.Remove("pageNumber");
                        string newQueryString = query.ToString();
                        string url = returnUrl.Substring(0, index + 1);
                        return Redirect($"{url}{newQueryString}");
                    }

                    return RedirectToAction("Index");
                }
            }

            return NotFound();
        }
    }
}
