using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using todo_aspnetcoremvc_ui.Models;
using todo_aspnetcoremvc_ui.Infrastructure;
using todo_aspnetcoremvc_ui.Models.ViewModels;
using todo_domain_entities.Models;
using todo_domain_entities.Interfaces;
using AutoMapper;

namespace todo_aspnetcoremvc_ui.Controllers
{
    [Authorize]
    public class TodoListController : Controller
    {
        private readonly ITodoListRepository _repo;
        private readonly ITodoEntryRepository _entryRepo;
        private readonly IUserProvider _userProvider;
        private readonly IMapper _mapper;
        public TodoListController(ITodoListRepository repo, IUserProvider userProvider, ITodoEntryRepository entryRepo, IMapper mapper)
        {
            _repo = repo;
            _userProvider = userProvider;
            _entryRepo = entryRepo;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index(ListIndexViewModel model)
        {
            var queryEntries = _repo.TodoLists
                .Include(x => x.TodoEntries)
                .Where(x => model.Category == null || x.Category == model.Category)
                .Where(x => x.ApplicationUserId == _userProvider.GetUserId())
                .SelectMany(x => x.TodoEntries);

            ListIndexViewViewModel viewModel = new ListIndexViewViewModel
            {
                CurrentSort = model.SortOrder,
                TitleSort = string.IsNullOrEmpty(model.SortOrder) ? "title_desc" : "",
                CategorySort = model.SortOrder == "cs" ? "cs_desc" : "cs",
                StatusSort = model.SortOrder == "ss" ? "ss_desc" : "ss",
                PrioritySort = model.SortOrder == "ps" ? "ps_desc" : "ps",
                Filter = model.Search,
                ShowHiddenLists = model.ShowHiddenLists,
                Category = model.Category,
                HideCompletedLists = model.HideCompletedLists,
                TotalEntries = queryEntries.Count(),
                DueTodayItemsCount = queryEntries.Where(x => x.DueDate.Day == DateTime.Now.Day).Count(),
                OverDueItemsCount = queryEntries.Where(x => x.DueDate < DateTime.Now && x.EntryStatus != Status.Completed).Count()
            };

            if (model.Search != null)
            {
                model.PageNumber = 1;
            }
            else
            {
                model.Search = model.CurrentFilter;
            }

            viewModel.CurrentFilter = model.Search;
            string userId = _userProvider.GetUserId();
            var list = _repo.TodoLists.Where(x => model.Category == null || x.Category == model.Category)
                .Where(x => x.ApplicationUserId == userId);

            if (!model.ShowHiddenLists)
            {
                list = list.Where(x => x.IsHidden != true);
            }

            if (model.HideListID != null)
            {
                var listToChangeVisibility = _repo.TodoLists.FirstOrDefault(x => x.ID == model.HideListID);
                if (listToChangeVisibility != null)
                {
                    listToChangeVisibility.IsHidden = !listToChangeVisibility.IsHidden;
                    bool successfull = await _repo.UpdateTodoListAsync(listToChangeVisibility);
                    if (!successfull)
                    {
                        return BadRequest("Could not change visibility");
                    }
                }
            }

            if (model.HideCompletedLists)
            {
                list = list.Where(x => x.TodoListStatus != Status.Completed);

            }

            if (!string.IsNullOrEmpty(model.Search))
            {
                list = list.Where(x => x.Category.Contains(model.Search) || x.Title.Contains(model.Search));
            }

            list = model.SortOrder switch
            {
                "title_desc" => list.OrderByDescending(x => x.Title),
                "cs" => list.OrderBy(x => x.Category),
                "cs_desc" => list.OrderByDescending(x => x.Category),
                "ss" => list.OrderBy(x => x.TodoListStatus),
                "ss_desc" => list.OrderByDescending(x => x.TodoListStatus),
                "ps" => list.OrderBy(x => x.TodoListPriority),
                "ps_desc" => list.OrderByDescending(x => x.TodoListPriority),
                _ => list.OrderBy(x => x.Title),
            };

            int pageSize = 5;

            viewModel.List = await PaginatedList<TodoList>.CreateAsync(list.AsNoTracking(), model.PageNumber ?? 1, pageSize);

            return View(viewModel);
        }

        public async Task<IActionResult> Copy(int id, string category)
        {
            var list = _repo.TodoLists.Include(x => x.TodoEntries).FirstOrDefault(x => x.ID == id);

            if (list != null)
            {
                var newList = new TodoList()
                {
                    ApplicationUserId = list.ApplicationUserId,
                    Category = list.Category,
                    Title = list.Title,
                    TodoListPriority = list.TodoListPriority,
                    TodoListStatus = list.TodoListStatus,
                };

                bool successfull = await _repo.CreateTodoListAsync(newList);
                if (!successfull)
                {
                    return BadRequest("Could not create copy");
                }

                if (list.TodoEntries.Count != 0)
                {
                    var newAddedList = _repo.TodoLists.Include(x => x.TodoEntries).OrderBy(x => x.ID).Last();
                    for (int i = 0; i < list.TodoEntries.Count; i++)
                    {
                        var entry = new TodoEntry
                        {
                            CreationDate = list.TodoEntries[i].CreationDate,
                            DueDate = list.TodoEntries[i].DueDate,
                            Description = list.TodoEntries[i].Description,
                            EntryPriority = list.TodoEntries[i].EntryPriority,
                            EntryStatus = list.TodoEntries[i].EntryStatus,
                            TodoListID = newAddedList.ID
                        };
                        bool successfullCreateEntry = await _entryRepo.CreateTodoEntryAsync(entry);
                        if (!successfullCreateEntry)
                        {
                            return BadRequest("Could not create copy");
                        }
                    }
                }

                if (!string.IsNullOrEmpty(category))
                {
                    if (_repo.TodoLists.Where(x => x.Category == category).FirstOrDefault() != null)
                    {
                        return RedirectToAction("Index", new { category = category });
                    }
                }

                return RedirectToAction("Index");
            }

            return NotFound();
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(TodoList list)
        {
            if (ModelState.IsValid)
            {
                string userId = _userProvider.GetUserId();
                list.ApplicationUserId = userId;
                bool successfull = await _repo.CreateTodoListAsync(list);
                if (!successfull)
                {
                    return BadRequest("Could not create new todo list");
                }

                return RedirectToAction("Index");
            }

            return View(list);
        }

        public async Task<IActionResult> Delete(int? id, string category)
        {
            if (id == null)
            {
                return NotFound();
            }

            var successfull = await _repo.DeleteTodoListAsync(id.Value);

            if (!successfull)
            {
                return BadRequest("Could not remove todo list");
            }

            if (!string.IsNullOrEmpty(category))
            {
                if (_repo.TodoLists.Where(x => x.Category == category).FirstOrDefault() != null)
                {
                    return RedirectToAction("Index", new { category = category });
                }         
            }

            return RedirectToAction("Index");
        }

        public IActionResult Update(int? id, string returnUrl)
        {
            if (id != null)
            {

                var todoList = _repo.TodoLists.FirstOrDefault(x => x.ID == id);

                if (todoList != null)
                {
                    var listUpdateViewModel = _mapper.Map<ListUpdateViewModel>(todoList);
                    listUpdateViewModel.ReturnUrl = returnUrl;

                    return View(listUpdateViewModel);
                }
            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Update(ListUpdateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var todoList = _repo.TodoLists.FirstOrDefault(x => x.ID == viewModel.ID);
                todoList = _mapper.Map(viewModel, todoList);
                todoList.ApplicationUserId = _userProvider.GetUserId();
                bool successfull = await _repo.UpdateTodoListAsync(todoList);
                if (!successfull)
                {
                    return BadRequest("Could not update todo list");
                }

                if (_repo.TodoLists.Where(x => x.Category == viewModel.ReturnUrl).FirstOrDefault() != null)
                {
                    return RedirectToAction("Index", new { category = viewModel.ReturnUrl });
                }

                return RedirectToAction("Index");
            }

            return View(viewModel);
        }
    }
}
