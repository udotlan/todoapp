﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_aspnetcoremvc_ui.Infrastructure;
using todo_domain_entities.Contexts;
using todo_domain_entities.Interfaces;
using todo_domain_entities.Models;
using todo_domain_entities.Services;

namespace todo_aspnetcoremvc_ui.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void ConfigureDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<TodoContext>(opts =>
            {
                opts.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
                opts.EnableSensitiveDataLogging(true);
            });
        }

        public static void ConfigureIdentity(this IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<TodoContext>();
        }

        public static void ConfigureServices(this IServiceCollection services)
        {
            services.AddScoped<ITodoListRepository, TodoListRepository>();
            services.AddScoped<ITodoEntryRepository, TodoEntryRepository>();
            services.AddScoped<IUserProvider, UserProvider>();
        }
    }
}
