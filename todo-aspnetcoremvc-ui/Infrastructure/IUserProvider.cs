﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using todo_domain_entities.Models;

namespace todo_aspnetcoremvc_ui.Infrastructure
{
    public interface IUserProvider
    {
        string GetUserId();
        Task<Tuple<bool, IEnumerable<string>>> CreateAsync(ApplicationUser user, string password);
        Task SignInAsync(ApplicationUser user, bool isPersistent);

        Task<bool> PasswordSignInAsync(string email, string password, bool rememberMe, bool lockOutOnFailure);
        Task SignOutAsync();
    }
}