﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_aspnetcoremvc_ui.Models.ViewModels;
using todo_domain_entities.Models;

namespace todo_aspnetcoremvc_ui.Infrastructure
{
    public class ItemUpdateMappingProfile : Profile
    {
        public ItemUpdateMappingProfile()
        {
            CreateMap<TodoEntry, ItemViewModel>().ReverseMap();     
        }
    }
}
