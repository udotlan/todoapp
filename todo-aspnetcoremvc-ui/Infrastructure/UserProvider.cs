﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_aspnetcoremvc_ui.Models;
using todo_domain_entities.Models;

namespace todo_aspnetcoremvc_ui.Infrastructure
{
    public class UserProvider:IUserProvider
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public UserProvider(UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContextAccessor, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
            _signInManager = signInManager;
        }

        public async Task<Tuple<bool, IEnumerable<string>>> CreateAsync(ApplicationUser user, string password)
        {
            var result = await _userManager.CreateAsync(user, password);
            if (result.Succeeded)
            {
                return Tuple.Create(true, new List<string>().AsEnumerable());
            }
            else
            {
                var listErrors = result.Errors.Select(x => x.Description).AsEnumerable();
                return Tuple.Create(false, listErrors);
            }
        }

        public string GetUserId()
        {
            string userId = _userManager.GetUserId(_httpContextAccessor.HttpContext.User);
            if (userId == null)
            {
                throw new ApplicationException("User not exist");
            }

            return userId;
        }

        public async Task<bool> PasswordSignInAsync(string email, string password, bool rememberMe, bool lockOutOnFailure)
        {
            var result = await _signInManager.PasswordSignInAsync(email, password, rememberMe, lockOutOnFailure);

            return result.Succeeded;
        }

        public async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            await _signInManager.SignInAsync(user, isPersistent);
        }

        public async Task SignOutAsync()
        {
            await _signInManager.SignOutAsync();
        }
    }
}
