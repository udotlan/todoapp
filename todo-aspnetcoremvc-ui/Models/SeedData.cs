﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;
using todo_domain_entities.Contexts;
using todo_domain_entities.Models;

namespace todo_aspnetcoremvc_ui.Models
{
    public class SeedData
    {

        public async static Task<ApplicationUser> FillUser(IApplicationBuilder app)
        {
            TodoContext userContext = app.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<TodoContext>();
            var userManager = app.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            
            if (userContext.Database.GetPendingMigrations().Any())
            {
                userContext.Database.Migrate();
            }
            
            string name = "Admin";
            string password = "12345!Admin";

            var admin = new ApplicationUser
            {
                UserName = name
            };

            var _user = await userManager.FindByNameAsync(admin.UserName);

            if (_user == null)
            {
                var createResult = await userManager.CreateAsync(admin, password);
                if (createResult.Succeeded)
                {
                    _user = await userManager.FindByNameAsync(admin.UserName);
                }

            }

            return _user;

        }
        public static void Filled(TodoContext context, IApplicationBuilder app)
        {
            var user = FillUser(app);

            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }

            if (context.TodoEntries.Count() == 0 && context.TodoLists.Count() == 0)
            {
                TodoList list1 = new TodoList
                {
                    Category = "Homeworks",
                    TodoListStatus = Status.NotStarted,
                    TodoListPriority = Priority.Important,
                    Title = "Home works",
                    ApplicationUserId = user.Result.Id
                };
                TodoList list2 = new TodoList
                {
                    Category = "Job",
                    TodoListStatus = Status.NotStarted,
                    TodoListPriority = Priority.Normal,
                    Title = "Job's stuff",
                    ApplicationUserId = user.Result.Id
                };
                TodoList list3 = new TodoList
                {
                    Category = "Sport",
                    TodoListStatus = Status.NotStarted,
                    TodoListPriority = Priority.Important,
                    Title = "Yraining",
                    ApplicationUserId = user.Result.Id
                };

                context.TodoLists.AddRange(list1, list2, list3);
                TodoEntry entry1 = new TodoEntry
                {
                    Description = "Home works",
                    CreationDate = DateTime.Now,
                    DueDate = DateTime.Now.AddDays(3),
                    EntryStatus = Status.NotStarted,
                    EntryPriority = Priority.Important,
                    TodoList = list1
                };
                TodoEntry entry2 = new TodoEntry
                {
                    Description = "Job's stuff",
                    CreationDate = DateTime.Now,
                    DueDate = DateTime.Now.AddDays(4),
                    EntryStatus = Status.NotStarted,
                    EntryPriority = Priority.Important,
                    TodoList = list2
                };
                TodoEntry entry3 = new TodoEntry
                {
                    Description = "Training",
                    CreationDate = DateTime.Now,
                    DueDate = DateTime.Now.AddDays(3),
                    EntryStatus = Status.NotStarted,
                    EntryPriority = Priority.Important,
                    TodoList = list3
                };

                context.TodoEntries.AddRange(entry1, entry2, entry3);
                context.SaveChanges();
            }
        }
    }
}
