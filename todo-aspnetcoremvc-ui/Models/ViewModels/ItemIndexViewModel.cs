﻿namespace todo_aspnetcoremvc_ui.Models.ViewModels
{
    public class ItemIndexViewModel
    {
        public int? Id { get; set; }
        public string SortOrder { get; set; }
        public string CurrentFilter { get; set; }
        public string Search { get; set; }
        public int? PageNumber { get; set; }
        public string Category { get; set; }
        public bool ShowDueTodayItems { get; set; }
        public bool ShowOverDueItems { get; set; }
        public string BackUrl { get; set; }
    }
}
