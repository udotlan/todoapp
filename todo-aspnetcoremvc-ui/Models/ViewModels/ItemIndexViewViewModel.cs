﻿using todo_domain_entities.Models;

namespace todo_aspnetcoremvc_ui.Models.ViewModels
{
    public class ItemIndexViewViewModel
    {
        public TodoList TodoList { get; set; }
        public string CurrentSort { get; set; }
        public string DescriptionSort { get; set; }
        public string CreationDateSort { get; set; }
        public string DueDateSort { get; set; }
        public string PrioritySort { get; set; }
        public string StatusSort { get; set; }
        public string TodoListSort { get; set; }
        public string CurrentFilter { get; set; }
        public bool ShowDueTodayItems { get; set; }
        public bool ShowOverDueItems { get; set; }
        public PaginatedList<TodoEntry> List { get; set; }
        public string BackUrl { get; set; }
        public int? ID { get; set; }
        public string Category { get; set; }
    }
}
