﻿using System;
using System.ComponentModel.DataAnnotations;
using todo_domain_entities.Models;
using todo_domain_entities.Services;

namespace todo_aspnetcoremvc_ui.Models.ViewModels
{
    public class ItemViewModel : IEquatable<ItemViewModel>
    {
        public int ID { get; set; }

        [Required]
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }

        [DateTime(ErrorMessage = "Due date must be greater than current date")]
        public DateTime DueDate { get; set; }
        public Status EntryStatus { get; set; }
        public Priority EntryPriority { get; set; }
        public int TodoListID { get; set; }
        public string ReturnUrl { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ItemViewModel)obj);
        }
        public override int GetHashCode()
        {
            return this.ID.GetHashCode() ^ this.TodoListID.GetHashCode();
        }
        public bool Equals(ItemViewModel other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;
            if (this.ID == other.ID && this.CreationDate == other.CreationDate && this.DueDate == other.DueDate
                && this.Description == other.Description && this.EntryPriority == other.EntryPriority
                && this.EntryStatus == other.EntryStatus && this.TodoListID == other.TodoListID
                && this.ReturnUrl == other.ReturnUrl)
            {
                return true;
            }

            return false;
        }
    }
}
