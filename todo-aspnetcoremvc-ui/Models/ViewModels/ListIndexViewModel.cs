﻿namespace todo_aspnetcoremvc_ui.Models.ViewModels
{
    public class ListIndexViewModel
    {
        public string Category { get; set; }
        public string SortOrder { get; set; }
        public string CurrentFilter { get; set; }
        public string Search { get; set; }
        public int? PageNumber { get; set; }
        public bool HideCompletedLists { get; set; }
        public int? HideListID { get; set; }
        public bool ShowHiddenLists { get; set; }
    }
}
