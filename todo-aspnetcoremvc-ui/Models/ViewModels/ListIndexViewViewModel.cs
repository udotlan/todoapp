﻿using todo_domain_entities.Models;

namespace todo_aspnetcoremvc_ui.Models.ViewModels
{
    public class ListIndexViewViewModel
    {
        public string CurrentSort { get; set; }
        public string TitleSort { get; set; }
        public string CategorySort { get; set; }
        public string StatusSort { get; set; }
        public string PrioritySort { get; set; }
        public string Filter { get; set; }
        public string Category { get; set; }
        public string CurrentFilter { get; set; }
        public bool HideCompletedLists { get; set; }
        public bool ShowHiddenLists { get; set; }
        public int TotalEntries { get; set; }
        public int DueTodayItemsCount { get; set; }
        public int OverDueItemsCount { get; set; }
        public PaginatedList<TodoList> List { get; set; }
    }
}
