﻿using System;
using System.ComponentModel.DataAnnotations;
using todo_domain_entities.Models;

namespace todo_aspnetcoremvc_ui.Models.ViewModels
{
    public class ListUpdateViewModel : IEquatable<ListUpdateViewModel>
    {
        public string ReturnUrl { get; set; }
        public int ID { get; set; }

        [Required]
        public string Category { get; set; }
        public Status TodoListStatus { get; set; }
        public Priority TodoListPriority { get; set; }
        public string ApplicationUserId { get; set; }

        [Required]
        public string Title { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ListUpdateViewModel)obj);
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode() ^ this.ApplicationUserId.GetHashCode();
        }

        public bool Equals(ListUpdateViewModel other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;
            if (this.ID == other.ID && this.Category == other.Category && this.TodoListStatus == other.TodoListStatus
                && this.TodoListPriority == other.TodoListPriority && this.ApplicationUserId == other.ApplicationUserId
                && this.ReturnUrl == other.ReturnUrl)
            {
                return true;
            }

            return false;
        }
    }
}
