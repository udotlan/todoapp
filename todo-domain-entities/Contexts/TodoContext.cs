﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using todo_domain_entities.Models;

namespace todo_domain_entities.Contexts
{
    public class TodoContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<TodoEntry> TodoEntries { get; set; }
        public DbSet<TodoList> TodoLists { get; set; }
        public TodoContext(DbContextOptions<TodoContext> dbContext) : base(dbContext)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TodoList>()
                .HasOne(x => x.ApplicationUser)
                .WithMany(y => y.TodoList)
                .HasForeignKey(x => x.ApplicationUserId)
                .OnDelete(DeleteBehavior.Cascade);
            base.OnModelCreating(modelBuilder);
        }
    }
}
