﻿using System;
using System.Linq;
using System.Reflection;

namespace todo_domain_entities.Extensions
{
    public static class DisplayNameAttribute
    { 
        public static TAttribute GetAttribute<TAttribute>(this Enum enumValue)
                where TAttribute : Attribute
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<TAttribute>();
        }
    }
}
