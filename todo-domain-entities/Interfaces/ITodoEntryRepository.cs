﻿using System.Linq;
using System.Threading.Tasks;
using todo_domain_entities.Models;

namespace todo_domain_entities.Interfaces
{
    public interface ITodoEntryRepository
    {
        IQueryable<TodoEntry> TodoEntries { get; }
        Task<bool> CreateTodoEntryAsync(TodoEntry entry);
        Task<bool> UpdateTodoEntryAsync(TodoEntry entry);
        Task<bool> DeleteTodoEntryAsync(int id);
    }
}
