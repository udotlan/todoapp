﻿using System.Linq;
using System.Threading.Tasks;
using todo_domain_entities.Models;

namespace todo_domain_entities.Interfaces
{
    public interface ITodoListRepository
    {
        IQueryable<TodoList> TodoLists { get; }
        Task<bool> CreateTodoListAsync(TodoList list);
        Task<bool> UpdateTodoListAsync(TodoList list);
        Task<bool> DeleteTodoListAsync(int id);
    }
}
