﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace todo_domain_entities.Models
{
    public class ApplicationUser : IdentityUser
    {
        public List<TodoList> TodoList { get; set; }
    }
}
