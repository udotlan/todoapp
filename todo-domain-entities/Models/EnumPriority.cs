﻿using System;
using System.Collections.Generic;
using System.Text;

namespace todo_domain_entities.Models
{
    public enum Priority
    {
        Low,
        Normal,
        Important,
        Critical
    }
}
