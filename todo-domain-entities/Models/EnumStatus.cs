﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace todo_domain_entities.Models
{
    public enum Status
    {
        [Display(Name = "Not started")]
        NotStarted,

        [Display(Name = "Completed")]
        Completed,

        [Display(Name = "In progress")]
        InProgress
        
    }
}
