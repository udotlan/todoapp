﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using todo_domain_entities.Services;

namespace todo_domain_entities.Models
{
    public class TodoEntry//:IValidatableObject
    {
        public int ID { get; set; }
        [Required]
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }

        [DateTime(ErrorMessage = "Due date must be greater than current date")]
        public DateTime DueDate { get; set; }
        public Status EntryStatus { get; set; }
        public Priority EntryPriority { get; set; }
        public int TodoListID { get; set; }
        public TodoList TodoList { get; set; }
    }
}
