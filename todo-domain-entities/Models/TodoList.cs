﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace todo_domain_entities.Models
{
    public class TodoList
    {
        public ApplicationUser ApplicationUser { get; set; }
        public string ApplicationUserId { get; set; }
        public int ID { get; set; }
        public List<TodoEntry> TodoEntries { get; set; } = new List<TodoEntry>();

        [Required]
        public string Category { get; set; }
        public Status TodoListStatus { get; set; }
        public Priority TodoListPriority { get; set; }

        [Required]
        public string Title { get; set; }
        public bool IsHidden { get; set; }
    }
}
