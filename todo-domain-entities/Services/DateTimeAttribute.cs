﻿using System;
using System.ComponentModel.DataAnnotations;

namespace todo_domain_entities.Services
{

    public class DateTimeAttribute:ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value != null && (DateTime)value > DateTime.Now)
            {
                return true;
            }

            return false;
        }

    }
}
