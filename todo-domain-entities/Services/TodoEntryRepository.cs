﻿using System.Linq;
using System.Threading.Tasks;
using todo_domain_entities.Contexts;
using todo_domain_entities.Interfaces;
using todo_domain_entities.Models;

namespace todo_domain_entities.Services
{
    public class TodoEntryRepository : ITodoEntryRepository
    {
        private readonly TodoContext _context;

        public TodoEntryRepository(TodoContext context)
        {
            _context = context;
        }

        public IQueryable<TodoEntry> TodoEntries => _context.TodoEntries;


        public async Task<bool> CreateTodoEntryAsync(TodoEntry entry)
        {
            _context.TodoEntries.Add(entry);
            int created = await _context.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> DeleteTodoEntryAsync(int id)
        {
            var entry = _context.TodoEntries.Find(id);
            int deleted = default;
            if (entry != null)
            {
                _context.TodoEntries.Remove(entry);
                deleted = await _context.SaveChangesAsync();
            }

            return deleted > 0;

        }

        public async Task<bool> UpdateTodoEntryAsync(TodoEntry entry)
        {
            _context.TodoEntries.Update(entry);
            int updated = await _context.SaveChangesAsync();
            return updated > 0;
        }
    }
}
