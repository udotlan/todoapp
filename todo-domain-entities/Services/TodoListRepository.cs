﻿using System.Linq;
using System.Threading.Tasks;
using todo_domain_entities.Contexts;
using todo_domain_entities.Interfaces;
using todo_domain_entities.Models;

namespace todo_domain_entities.Services
{
    public class TodoListRepository:ITodoListRepository
    {
        private readonly TodoContext _context;

        public TodoListRepository(TodoContext context)
        {
           _context = context;
        }

        public IQueryable<TodoList> TodoLists => _context.TodoLists;

        public async Task<bool> CreateTodoListAsync(TodoList list)
        {
            _context.TodoLists.Add(list);

            int saved = await _context.SaveChangesAsync();
            return saved > 0;
        }

        public async Task<bool> DeleteTodoListAsync(int id)
        {
            int saved = default;
            var list = _context.TodoLists.FirstOrDefault(x => x.ID == id);
            if (list != null)
            {
                _context.TodoLists.Remove(list);
                saved = await _context.SaveChangesAsync();
            }

            return saved > 0;
        }

        public async Task<bool> UpdateTodoListAsync(TodoList list)
        {
            _context.TodoLists.Update(list);
            var saved = await _context.SaveChangesAsync();
            return saved > 0;
        }
    }
}
