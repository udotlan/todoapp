﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_aspnetcoremvc_ui.Controllers;
using todo_aspnetcoremvc_ui.Infrastructure;
using todo_aspnetcoremvc_ui.Models.ViewModels;
using todo_domain_entities.Models;
using Xunit;

namespace todo_test
{
    public class AccountControllerTests
    {
        private readonly Mock<IUserProvider> _userProviderMock;
        private readonly AccountController _controller;

        public AccountControllerTests()
        {
            _userProviderMock = new Mock<IUserProvider>();
            _controller = new AccountController(_userProviderMock.Object);
        }

        [Fact]
        public void RegisterTest_ReturnsViewResult()
        {
            // Act
            var result = _controller.Register();

            // Assert 
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async Task RegisterTest_ReturnsViewResult_WhenModelIsNotValid()
        {
            // Arrange
            _controller.ModelState.AddModelError("Error", "Error");
            RegisterViewModel viewModel = new RegisterViewModel()
            {
                Name = "email",
                Password = "password",
                PasswordConfirm = "password",
            };

            // Act
            var result = await _controller.Register(viewModel);

            // Assert
            Assert.IsType<ViewResult>(result);
            Assert.Equal(viewModel, (RegisterViewModel)((ViewResult)result).ViewData.Model);
            
        }

        [Fact]
        public async Task RegisterTest_ReturnsRedirectToAction_WhenUserIsCreated()
        {
            // Arrange
            var viewModel = new RegisterViewModel()
            {
                Name = "email",
                Password = "Email123!",
                PasswordConfirm = "Email123!"
            };
            var tuple = Tuple.Create<bool, IEnumerable<string>>(true, new List<string>().AsEnumerable());
            _userProviderMock.Setup(x => x.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(tuple);

            // Act
            var result = await _controller.Register(viewModel);

            // Assert
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Index", viewResult.ActionName);
            Assert.Equal("Home", viewResult.ControllerName);
        }
        
        [Fact]
        public async Task RegisterTest_ReturnsViewResult_WhenUserIsNotCreated()
        {
            // Arrange
            var viewModel = new RegisterViewModel()
            {
                Name = "email",
                Password = "Email123!",
                PasswordConfirm = "Email123!"
            };
            var tuple = Tuple.Create(false, new string[] { "Error1", "Error2", "Error3" }.AsEnumerable());
            _userProviderMock.Setup(x => x.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>())).ReturnsAsync(tuple);

            // Act
            var result = await _controller.Register(viewModel);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal(viewModel, viewResult.ViewData.Model);
            Assert.Equal(viewResult.ViewData.ModelState.ErrorCount, tuple.Item2.Count());
        }

        [Fact]
        public void LoginTest_ReturnsViewResult()
        {
            // Arrange
            string returnUrl = "return.Url";

            // Act
            var result = _controller.Login(returnUrl);
            var model = (LoginViewModel)((ViewResult)result).ViewData.Model;

            // Assert
            Assert.IsType<ViewResult>(result);
            Assert.Equal(returnUrl, model.ReturnUrl);
        }

        [Fact]
        public async Task LoginTest_ReturnsViewResult_WhenLoginViewModelIsNotValid()
        {
            // Arrange
            _controller.ModelState.AddModelError("Error", "Error");
            var viewModel = new LoginViewModel()
            {
                Name = "email",
                Password = "password",
                RememberMe = true,
                ReturnUrl = "returUrl"
            };

            // Act
            var result = await _controller.Login(viewModel);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal(viewModel, viewResult.ViewData.Model);
        }

        [Fact]
        public async Task LoginTest_ReturnsRedirectResult_WhenReturnUrlIsNotNullOrEmtpy()
        {
            // Arrange
            string returnUrl = "return.Url";
            var mockUrlHelper = new Mock<IUrlHelper>();
            mockUrlHelper.Setup(x => x.IsLocalUrl(It.IsAny<string>())).Returns(true);
            _controller.Url = mockUrlHelper.Object;
            _userProviderMock.Setup(x => x.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<bool>(), It.IsAny<bool>())).ReturnsAsync(true);

            // Act
            var result = await _controller.Login(new LoginViewModel { ReturnUrl = returnUrl });

            // Assert
            var viewResult = Assert.IsType<RedirectResult>(result);
            Assert.Equal(returnUrl, viewResult.Url);
        }

        [Fact]
        public async Task LoginTest_ReturnsRedirectToActionResult_WhenReturnUrlIsNotLocal()
        {
            // Arrange
            string returnUrl = "return.Url";
            var mockUrlHelper = new Mock<IUrlHelper>();
            mockUrlHelper.Setup(x => x.IsLocalUrl(It.IsAny<string>())).Returns(false);
            _controller.Url = mockUrlHelper.Object;
            _userProviderMock.Setup(x => x.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<bool>(), It.IsAny<bool>())).ReturnsAsync(true);

            // Act
            var result = await _controller.Login(new LoginViewModel { ReturnUrl = returnUrl });

            // Assert
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Home", viewResult.ControllerName);
            Assert.Equal("Index", viewResult.ActionName);
        }
        
        [Fact]
        public async Task LoginTest_ReturnsRedirectToActionResult_WhenReturnUrlIsNullOrEmpty()
        {
            // Arrange
            string returnUrl = null;
            var mockUrlHelper = new Mock<IUrlHelper>();
            mockUrlHelper.Setup(x => x.IsLocalUrl(It.IsAny<string>())).Returns(true);
            _controller.Url = mockUrlHelper.Object;
            _userProviderMock.Setup(x => x.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<bool>(), It.IsAny<bool>())).ReturnsAsync(true);

            // Act
            var result = await _controller.Login(new LoginViewModel { ReturnUrl = returnUrl });

            // Assert
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Home", viewResult.ControllerName);
            Assert.Equal("Index", viewResult.ActionName);
        }

        [Fact]
        public async Task LoginTest_ReturnsViewResult_WhenUserIsNotSignedIn()
        {
            // Arrange
            _userProviderMock.Setup(x => x.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<bool>(), It.IsAny<bool>())).ReturnsAsync(false);
            var viewModel = new LoginViewModel()
            {
                Name = "email",
                Password = "password",
                RememberMe = true,
                ReturnUrl = "returUrl"
            };

            // Act
            var result = await _controller.Login(viewModel);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal(viewModel, viewResult.ViewData.Model);
            Assert.Equal("Login or password is not valid", viewResult.ViewData.ModelState.Values
                .SelectMany(x => x.Errors).FirstOrDefault().ErrorMessage);
        }

        [Fact]
        public async Task LogoutTest_ReturnsRedirectToActionResult()
        {
            // Act
            var result = await _controller.Logout();

            // Assert
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Home", viewResult.ControllerName);
            Assert.Equal("Index", viewResult.ActionName);
        }
    }
}
