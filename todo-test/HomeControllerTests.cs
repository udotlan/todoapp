﻿using Microsoft.AspNetCore.Mvc;
using todo_aspnetcoremvc_ui.Controllers;
using Xunit;

namespace todo_test
{
    public class HomeControllerTests
    {
        [Fact]
        public void IndexTest()
        {
            // Act
            var result = new HomeController().Index();

            // Assert
            Assert.IsType<ViewResult>(result);
        }
    }
}
