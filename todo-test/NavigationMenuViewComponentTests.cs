﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.AspNetCore.Routing;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;
using todo_aspnetcoremvc_ui.Components;
using todo_aspnetcoremvc_ui.Infrastructure;
using todo_domain_entities.Interfaces;
using Xunit;

namespace todo_test
{
    public class NavigationMenuViewComponentTests
    {
        private readonly Mock<ITodoListRepository> _listrepositoryMock;
        private readonly Mock<IUserProvider> _userProviderMock;
        private readonly NavigationMenuViewComponent _component;

        public NavigationMenuViewComponentTests()
        {
            _listrepositoryMock = new Mock<ITodoListRepository>();
            _userProviderMock = new Mock<IUserProvider>();
            _component = new NavigationMenuViewComponent(_listrepositoryMock.Object, _userProviderMock.Object);
        }

        [Fact]
        public void InvokeTest()
        {
            // Arrange
            string userID = "1";
            var todoLists = new TodoListControllerTests().GetTodoListList();
            _userProviderMock.Setup(x => x.GetUserId()).Returns(userID);
            var mock = todoLists.AsQueryable().BuildMock();
            _listrepositoryMock.Setup(x => x.TodoLists).Returns(mock.Object);
            _component.ViewComponentContext = new ViewComponentContext()
            {
                ViewContext = new ViewContext()
                {
                    RouteData = new RouteData(),
                    HttpContext = new DefaultHttpContext()
                }, 
            };
            var query = new Dictionary<string, string>
            {
                { "showHiddenLists", "true" },
                { "hideCompletedLists", "true" }
            };
            _component.Request.QueryString = QueryString.Create(query);

            // Act
            var result = (ViewViewComponentResult)_component.Invoke();
            var expectedCategories = todoLists
                .Where(x => x.ApplicationUserId == userID)
                .Select(x => x.Category)
                .Distinct()
                .OrderBy(x => x)
                .ToList();
            // Assert
            Assert.Equal(expectedCategories, result.ViewData["Categories"]);
            Assert.Equal(query["showHiddenLists"], result.ViewData["showHiddenLists"].ToString());
            Assert.Equal(query["hideCompletedLists"], result.ViewData["hideCompletedLists"].ToString());
        }
    }
}
