﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_aspnetcoremvc_ui.Controllers;
using todo_aspnetcoremvc_ui.Infrastructure;
using todo_aspnetcoremvc_ui.Models.ViewModels;
using todo_domain_entities.Interfaces;
using todo_domain_entities.Models;
using Xunit;

namespace todo_test
{
    public class TodoEntryControllerTests
    {
        private readonly Mock<ITodoEntryRepository> _repositoryMock;
        private readonly Mock<ITodoListRepository> _listrepositoryMock;
        private readonly TodoEntryController _controller;
        private readonly IMapper _mapper;
        private readonly Mock<IUserProvider> _userProvider;
        public TodoEntryControllerTests()
        {
            _repositoryMock = new Mock<ITodoEntryRepository>();
            _listrepositoryMock = new Mock<ITodoListRepository>();
            _userProvider = new Mock<IUserProvider>();
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ItemUpdateMappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            _mapper = mapper;
            _controller = new TodoEntryController(_repositoryMock.Object, _listrepositoryMock.Object, _mapper, _userProvider.Object);
        }

        #region IndexTest

        [Fact]
        public async Task IndexTest_WithoutPassingArguments()
        {
            // Arrange
            var todoEntries = GetTodoEntryList();
            var mock = todoEntries.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoEntries).Returns(mock.Object);
            _userProvider.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ItemIndexViewModel());

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result); 
            var model = Assert.IsAssignableFrom<ItemIndexViewViewModel>(
                viewResult.ViewData.Model);
            Assert.Equal(5, model.List.Count());
        }
        
        [Fact]
        public async Task IndexTest_WithPassingID()
        {
            // Arrange
            var todoList = GetTodoListList().First(); ;
            var todoEntries = GetTodoEntryList();
            var mock = todoEntries.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoEntries).Returns(mock.Object);
            _listrepositoryMock.Setup(x => x.TodoLists).Returns(((new TodoList[] { todoList }).AsQueryable()));

            // Act
            var result = await _controller.Index(new ItemIndexViewModel() { Id = todoList.ID });
            var expectedCount = todoEntries.Where(x => x.TodoListID == todoList.ID).Count();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result); 
            var model = Assert.IsAssignableFrom<ItemIndexViewViewModel>(
                viewResult.ViewData.Model);
            Assert.Equal(expectedCount, model.List.Count());
            Assert.Equal(todoList, model.TodoList);
        }

        [Fact]
        public async Task IndexTest_WithPassingSortOrder()
        {
            // Arrange
            string sortOrder = "desc_desc";
            var todoEntries = GetTodoEntryList();
            var mock = todoEntries.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoEntries).Returns(mock.Object);
            _userProvider.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ItemIndexViewModel() { SortOrder = sortOrder });
            var expected = todoEntries.OrderByDescending(x => x.Description).Take(5);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<ItemIndexViewViewModel>(
                viewResult.ViewData.Model);
            var actual = model.List;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task IndexTest_WithPassingSearchString()
        {
            // Arrange
            string search = "3";
            var todoEntries = GetTodoEntryList();
            var mock = todoEntries.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoEntries).Returns(mock.Object);
            _userProvider.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ItemIndexViewModel() { Search = search });
            var expected = todoEntries.Where(x => x.Description.Contains(search));
            var model = (ItemIndexViewViewModel)((ViewResult)result).ViewData.Model;
           

            // Assert
            Assert.Equal(expected, model.List);
        }

        [Fact]
        public async Task IndexTest_WithPassingPageNumber()
        {
            // Arrange
            int pageNumber = 2;
            var todoEntries = GetTodoEntryList();
            var mock = todoEntries.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoEntries).Returns(mock.Object);
            _userProvider.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ItemIndexViewModel() { PageNumber = pageNumber });
            var expected = todoEntries.OrderBy(x => x.Description).Skip(5);
            var model = (ItemIndexViewViewModel)((ViewResult)result).ViewData.Model;
            var actual = model.List;

            // Assert
            Assert.Equal(expected, actual);
            Assert.False(model.List.HasNextPage);
            Assert.True(model.List.HasPreviousePage);
            Assert.Equal(pageNumber, model.List.PageIndex);
        }
        
        [Fact]
        public async Task IndexTest_WithPassingCategory()
        {
            // Arrange
            string category = "3";
            var todoEntries = GetTodoEntryList();
            var todoLists = GetTodoListList();
            var entryMock = todoEntries.AsQueryable().BuildMock();
            var listMock = todoLists.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoEntries).Returns(entryMock.Object);
            _listrepositoryMock.Setup(x => x.TodoLists).Returns(listMock.Object);
            _userProvider.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ItemIndexViewModel() { Category = category });
            var selectedTodoListsID = todoLists.Where(x => x.Category == category).Select(x => x.ID);
            var expected = todoEntries.Where(x => selectedTodoListsID.Any(y => y == x.TodoListID));
            var model = (ItemIndexViewViewModel)((ViewResult)result).ViewData.Model;
            var actual = model.List;

            // Assert
            Assert.Equal(expected, actual);
            Assert.Equal(category, model.Category);
        }

        [Fact]
        public async Task IndexTest_WithPassingShowDueTodayItems()
        {
            // Arrange
            bool showDueTodayItems = true;
            var todoEntries = GetTodoEntryList();
            var entryMock = todoEntries.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoEntries).Returns(entryMock.Object);
            _userProvider.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ItemIndexViewModel() { ShowDueTodayItems = showDueTodayItems });
            var expected = todoEntries.Where(x => x.DueDate.Date == DateTime.Now.Date);
            var model = (ItemIndexViewViewModel)((ViewResult)result).ViewData.Model;
            var actual = model.List;

            // Assert
            Assert.Equal(expected, actual);
            Assert.Equal(showDueTodayItems, model.ShowDueTodayItems);
        }

        [Fact]
        public async Task IndexTest_WithPassingShowOverDueItems()
        {
            // Arrange
            bool showOverDueItems = true;
            var todoEntries = GetTodoEntryList();
            var entryMock = todoEntries.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoEntries).Returns(entryMock.Object);
            _userProvider.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ItemIndexViewModel() { ShowOverDueItems = showOverDueItems });
            var expected = todoEntries.Where(x => x.DueDate < DateTime.Now && x.EntryStatus != Status.Completed);
            var model = (ItemIndexViewViewModel)((ViewResult)result).ViewData.Model;
            var actual = model.List;

            // Assert
            Assert.Equal(expected, actual);
            Assert.Equal(showOverDueItems, model.ShowOverDueItems);
        }

        [Fact]
        public async Task IndexTest_WithPassingBackUrl()
        {
            // Arrange
            string backUrl = "backUrl.com";
            var todoEntries = GetTodoEntryList();
            var entryMock = todoEntries.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoEntries).Returns(entryMock.Object);
            _userProvider.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ItemIndexViewModel() { BackUrl = backUrl });
            var model = (ItemIndexViewViewModel)((ViewResult)result).ViewData.Model;

            // Assert
            Assert.Equal(backUrl, model.BackUrl);
        }

        #endregion IndexTest

        #region AddTest

        [Fact]
        public async Task AddTest_ReturnViewResult_WhenTodoEntryIsNotValid()
        {
            // Arrange
            _controller.ModelState.AddModelError("Error", "Error");
            var viewModel = GetItemViewModel();

            // Act
            var result = await _controller.Add(viewModel);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal("_FormPartialView", viewResult.ViewName);
        }

        [Fact]
        public async Task AddTest_AddsTodoListToRepository_And_RedirectsToUrl()
        {
            // Arrange
            var viewModel = GetItemViewModel(); 
            _repositoryMock.Setup(x => x.CreateTodoEntryAsync(It.IsAny<TodoEntry>())).ReturnsAsync(true);

            // Act
            var result = await _controller.Add(viewModel);

            // Assert
            _repositoryMock.Verify(x => x.CreateTodoEntryAsync(It.IsAny<TodoEntry>()), Times.Once);
            var viewResult = Assert.IsType<RedirectResult>(result);
            Assert.Equal(viewModel.ReturnUrl, viewResult.Url);
        }
        /*
        [Fact]
        public void AddTest_SetsCreationDateAndTodoListStatus_BeforeAddingArticleToRepository()
        {
            // Arrange
            var dateTime = new DateTime(2022, 01, 01);
            var todoEntry = new TodoEntry { ID = 1, Description = "1", TodoListID = 1, CreationDate = dateTime };
            var viewModel = _mapper.Map<ItemViewModel>(todoEntry);

            // Act
            var result = _controller.Add(viewModel);

            // Assert
            _repositoryMock.Verify(x => x.CreateTodoEntryAsync(It.Is<TodoEntry>(x => x == todoEntry
                && x.CreationDate != dateTime
                && x.EntryStatus == Status.NotStarted)), Times.Once);
        }
        */
        [Fact]
        public async Task AddTest_ReturnsBadRequestObjectResult_WhenTodoEntryIsNotCreated()
        {
            // Arrange
            var viewModel = GetItemViewModel();
            _repositoryMock.Setup(x => x.CreateTodoEntryAsync(It.IsAny<TodoEntry>())).ReturnsAsync(false);

            // Act

            var result = await _controller.Add(viewModel);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task AddTest_ReturnsRedirectToActionResult_WhenReturnUrlIsNullOrEmpty()
        {
            // Arrange
            var viewModel = GetItemViewModel();
            viewModel.ReturnUrl = string.Empty;
            _repositoryMock.Setup(x => x.CreateTodoEntryAsync(It.IsAny<TodoEntry>())).ReturnsAsync(true);

            // Act
            var result = await _controller.Add(viewModel);

            // Assert
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Index", viewResult.ActionName);
        }

        #endregion AddTest

        #region UpdateTest

        [Fact]

        public void UpdateTest_ReturnsNotFoundResult_WhenIdIsNull()
        {
            // Act
            var result = _controller.Update(null, string.Empty);

            // Assert
            Assert.IsType<NotFoundResult>(result);

        }

        [Fact]
        public void UpdateTest_ReturnsNotFoundResult_WhenTodoEntryIsNotExist()
        {
            // Act
            var result = _controller.Update(1, string.Empty);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void UpdateTest_ReturnsViewResult_WhenTodoEntryIsExist()
        {
            // Arrange
            string returnUrl = string.Empty;
            var todoEntry = GetTodoEntryList().First();
            _repositoryMock.Setup(x => x.TodoEntries).Returns((new TodoEntry[] { todoEntry }).AsQueryable());

            // Act
            var result = _controller.Update(todoEntry.ID, returnUrl);
            var viewModel = _mapper.Map<ItemViewModel>(todoEntry);
            viewModel.ReturnUrl = returnUrl;

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<ItemViewModel>(
                viewResult.ViewData.Model);
            Assert.Equal(viewModel, model);
        }

        [Fact]
        public async Task UpdateTest_ReturnsViewResult_WhentTodoEntryIsNotValid()
        {
            // Arrange
            _controller.ModelState.AddModelError("Error", "Error");
            var viewModel = GetItemViewModel();

            // Act
            var result = await _controller.Update(viewModel);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<ItemViewModel>(
                viewResult.ViewData.Model);
            Assert.Equal(viewModel, model);
        }

        [Fact]
        public async Task UpdateTest_ReturnsBadRequestObjectResult_WhenTodoEntryIsNotUpdated()
        {
            // Arrange
            var viewModel = GetItemViewModel();
            _repositoryMock.Setup(x => x.UpdateTodoEntryAsync(It.IsAny<TodoEntry>())).ReturnsAsync(false);

            // Act
            var result = await _controller.Update(viewModel);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Could not change item",
                (result as BadRequestObjectResult).Value.ToString());
        }

        [Fact]
        public async Task UpdateTest_UpdateTodoEntry_And_ReturnsRedirectResult_WhenReturnUrlIsNotNullOrEmpty()
        {
            // Arrange
            var viewModel = GetItemViewModel();

            _repositoryMock.Setup(x => x.UpdateTodoEntryAsync(It.IsAny<TodoEntry>())).ReturnsAsync(true);

            // Act
            var result = await _controller.Update(viewModel);

            // Assert
            _repositoryMock.Verify(x => x.UpdateTodoEntryAsync(It.Is<TodoEntry>(x =>
                x.ID == viewModel.ID && x.TodoListID == viewModel.TodoListID && x.CreationDate == viewModel.CreationDate
                && x.DueDate == viewModel.DueDate && x.Description == viewModel.Description
                && x.EntryPriority == viewModel.EntryPriority && x.EntryStatus == viewModel.EntryStatus)));
            var viewResult = Assert.IsType<RedirectResult>(result);
            Assert.Equal(viewModel.ReturnUrl, viewResult.Url);
        }

        [Fact]
        public async Task UpdateTest_UpdateTodoEntry_And_ReturnsRedirectToActionResult_WhenReturnUrlIsNullOrEmpty()
        {
            // Arrange
            var viewModel = GetItemViewModel();
            viewModel.ReturnUrl = string.Empty;
            _repositoryMock.Setup(x => x.UpdateTodoEntryAsync(It.IsAny<TodoEntry>())).ReturnsAsync(true);

            // Act
            var result = await _controller.Update(viewModel);

            // Assert
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Index", viewResult.ActionName);
        }

        #endregion UpdateTest

        #region DeleteTest

        [Fact]

        public async Task DeleteTest_ReturnsNotFoundReuslt_WhenIdIsNull()
        {
            // Act
            var result = await _controller.Delete(null, string.Empty, 1);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task DeleteTest_ReturnsNotFoundResult_WhenTodoEntryIsNotExist()
        {
            // Act
            var result = await _controller.Delete(1, string.Empty, 1);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task DeleteTest_ReturnsBadRequest_WhenTodoEntryIsNotDeleted()
        {
            // Arrange
            var todoEntry = GetTodoEntryList().First();
            _repositoryMock.Setup(x => x.TodoEntries).Returns((new TodoEntry[] { todoEntry }).AsQueryable());
            _repositoryMock.Setup(x => x.DeleteTodoEntryAsync(It.IsAny<int>())).ReturnsAsync(false);
            // Act

            var result = await _controller.Delete(1, string.Empty, 1);

            // Assert
            var viewResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Could not remove item",
                (result as BadRequestObjectResult).Value.ToString()
            );
        }

        [Fact]
        public async Task DeleteTest_DeleteTodoList_AndReturnsRedirectToActionResult_WhenReturnUrlIsNullOrEmpty()
        {
            // Arrange
             var todoEntry = GetTodoEntryList().First();
            _repositoryMock.Setup(x => x.TodoEntries).Returns((new TodoEntry[] { todoEntry }).AsQueryable());
            _repositoryMock.Setup(x => x.DeleteTodoEntryAsync(It.IsAny<int>())).ReturnsAsync(true);
            // Act

            var result = await _controller.Delete(todoEntry.ID, string.Empty, 1);

            // Assert
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Index", viewResult.ActionName);
        }

        [Fact]
        public async Task DeleteTest_DeleteTodoList_And_ReturnsRedirectResult_WhenReturnUrlIsNotNullOrEmpty()
        {
            // Arrange
            string returnUrl = "return.url";
            var todoEntry = GetTodoEntryList().First();
            _repositoryMock.Setup(x => x.TodoEntries).Returns((new TodoEntry[] { todoEntry }).AsQueryable());
            _repositoryMock.Setup(x => x.DeleteTodoEntryAsync(It.IsAny<int>())).ReturnsAsync(true);
            // Act

            var result = await _controller.Delete(todoEntry.ID, returnUrl, 1);

            // Assert
            _repositoryMock.Verify(x => x.DeleteTodoEntryAsync(todoEntry.ID));
            var viewResult = Assert.IsType<RedirectResult>(result);
            Assert.Equal(returnUrl, viewResult.Url);
        }
        
        [Fact]
        public async Task DeleteTest_DeleteTodoList_And_ReturnsRedirectResult_WithRemovingPageNumberQueryParameterFromReturnUrl()
        {
            // Arrange
            string returnUrl = "return.url?pageNumber=1&&search=1";
            var todoEntry = GetTodoEntryList().First();
            _repositoryMock.Setup(x => x.TodoEntries).Returns((new TodoEntry[] { todoEntry }).AsQueryable());
            _repositoryMock.Setup(x => x.DeleteTodoEntryAsync(It.IsAny<int>())).ReturnsAsync(true);

            // Act
            var result = await _controller.Delete(todoEntry.ID, returnUrl, 1);
            var indexStart = returnUrl.IndexOf("pageNumber");
            var indexEnd = returnUrl.IndexOf("&");
            var exptected = returnUrl.Remove(indexStart, indexEnd - indexStart + 1);

            // Assert
            var viewResult = Assert.IsType<RedirectResult>(result);
            Assert.Equal(exptected, viewResult.Url);
        }

        #endregion DeleteTest

        #region Private methods

        private List<TodoEntry> GetTodoEntryList()
        {
            var todoLists = GetTodoListList();

            var todoEntries = new List<TodoEntry>()
            {
                 new TodoEntry { ID = 1, Description = "1", DueDate = DateTime.Now, EntryStatus = Status.Completed, TodoListID = 1, TodoList = todoLists.First() },
                 new TodoEntry { ID = 2, Description = "2", DueDate = DateTime.Now.AddDays(-1), EntryStatus = Status.InProgress,  TodoListID = 2, TodoList = todoLists.Skip(1).First() },
                 new TodoEntry { ID = 3, Description = "3", DueDate = DateTime.Now.AddDays(1), EntryStatus = Status.NotStarted,  TodoListID = 3, TodoList = todoLists.Skip(2).First() },
                 new TodoEntry { ID = 4, Description = "4", DueDate = DateTime.Now, EntryStatus = Status.Completed,  TodoListID = 4, TodoList = todoLists.Skip(3).First() },
                 new TodoEntry { ID = 5, Description = "5", DueDate = DateTime.Now.AddDays(-1), EntryStatus = Status.InProgress,  TodoListID = 5, TodoList = todoLists.Skip(4).First()  },
                 new TodoEntry { ID = 6, Description = "6", DueDate = DateTime.Now.AddDays(1), EntryStatus = Status.NotStarted,  TodoListID = 6, TodoList = todoLists.Skip(5).First() },
                 new TodoEntry { ID = 7, Description = "7", DueDate = DateTime.Now, EntryStatus = Status.Completed,  TodoListID = 7, TodoList = todoLists.Last()  },
            };

            return todoEntries;
        }

        private List<TodoList> GetTodoListList()
        {
            var todolists = new List<TodoList>()
            {
                new TodoList { ID = 1, Category = "1", IsHidden = true, TodoListStatus = Status.Completed, ApplicationUserId = "1" },
                new TodoList { ID = 2, Category = "2", IsHidden = false, TodoListStatus = Status.InProgress, ApplicationUserId = "1"  },
                new TodoList { ID = 3, Category = "3", IsHidden = true, TodoListStatus = Status.NotStarted, ApplicationUserId = "1"  },
                new TodoList { ID = 4, Category = "3", IsHidden = true, TodoListStatus = Status.NotStarted, ApplicationUserId = "1"  },
                new TodoList { ID = 5, Category = "3", IsHidden = true, TodoListStatus = Status.NotStarted, ApplicationUserId = "1"  },
                new TodoList { ID = 6, Category = "3", IsHidden = true, TodoListStatus = Status.NotStarted, ApplicationUserId = "1"  },
                new TodoList { ID = 7, Category = "3", IsHidden = true, TodoListStatus = Status.NotStarted, ApplicationUserId = "1"  },
            };

            return todolists;
        }

        private ItemViewModel GetItemViewModel()
        {
            var viewModel = new ItemViewModel()
            {
                ID = 1,
                CreationDate = DateTime.Now,
                DueDate = DateTime.Now.AddDays(1),
                Description = "New entry",
                EntryPriority = Priority.Low,
                EntryStatus = Status.NotStarted,
                TodoListID = 1,
                ReturnUrl = "returnUrl.com"
            };

            return viewModel;
        }

        #endregion Private methods
    }
}

