using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_aspnetcoremvc_ui.Controllers;
using todo_aspnetcoremvc_ui.Infrastructure;
using todo_aspnetcoremvc_ui.Models.ViewModels;
using todo_domain_entities.Interfaces;
using todo_domain_entities.Models;
using Xunit;

namespace todo_test
{
    public class TodoListControllerTests
    {
        private readonly Mock<ITodoListRepository> _repositoryMock;
        private readonly Mock<IUserProvider> _userProviderMock;
        private readonly Mock<ITodoEntryRepository> _entryRepoMock;
        private readonly IMapper _mapper;
        private readonly TodoListController _controller;

        public TodoListControllerTests()
        {
            _repositoryMock = new Mock<ITodoListRepository>();
            _userProviderMock = new Mock<IUserProvider>();
            _entryRepoMock = new Mock<ITodoEntryRepository>();
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ListUpdateMappingProfile());
            });

            _mapper = mappingConfig.CreateMapper(); ;
            _controller = new TodoListController(_repositoryMock.Object, _userProviderMock.Object, _entryRepoMock.Object, _mapper);
        }
         
        #region IndexTest

        [Fact]
        public async Task IndexTest_WithoutPassingArguments()
        {
            // Arrange
            var todoLists = GetTodoListList();
            var mock = todoLists.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoLists).Returns(mock.Object);
            _userProviderMock.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ListIndexViewModel());
            var expected = todoLists.Where(x => x.IsHidden != true).OrderBy(x => x.Title);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<ListIndexViewViewModel>(
                viewResult.ViewData.Model);
            Assert.Equal(expected, model.List);
        }

        [Fact]
        public async Task IndexTest_WithPassingCategory()
        {
            // Arrange
            string category = "Sport";
            var todoLists = GetTodoListList();
            var mock = todoLists.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoLists).Returns(mock.Object);
            _userProviderMock.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ListIndexViewModel() { Category = category });
            var expectedCount = todoLists.Where(x => x.Category == category).Where(x => x.IsHidden != true);
            var model = (ListIndexViewViewModel)((ViewResult)result).ViewData.Model;

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal(category, model.List.First().Category);
            Assert.Equal(expectedCount, model.List);
        }

        [Fact]
        public async Task IndexTest_WithPassingSortOrder()
        {
            // Arrange
            string sortOrder = "cs";
            var todoLists = GetTodoListList();
            var mock = todoLists.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoLists).Returns(mock.Object);
            _userProviderMock.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ListIndexViewModel() { SortOrder = sortOrder });
            var expected = todoLists.Where(x => x.IsHidden != true).OrderBy(x => x.Category);
            var model = (ListIndexViewViewModel)((ViewResult)result).ViewData.Model;

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal(sortOrder, model.CurrentSort);
            Assert.Equal(expected, model.List);
        }

        [Fact]
        public async Task IndexTest_WithPassingSearchString()
        {
            // Arrange
            string search = "sport";
            var todoLists = GetTodoListList();
            var mock = todoLists.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoLists).Returns(mock.Object);
            _userProviderMock.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ListIndexViewModel() { Search = search });
            var expected = todoLists.Where(x => x.Category.Contains(search) || x.Title.Contains(search)).Where(x => x.IsHidden != true);
            var model = (ListIndexViewViewModel)((ViewResult)result).ViewData.Model;

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal(search, model.CurrentFilter);
            Assert.Equal(expected, model.List);
        }

        [Fact]
        public async Task IndexTest_WithPassingPageNumber()
        {
            // Arrange
            int pageNumber = 3;
            var todoLists = GetTodoListList();
            var mock = todoLists.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoLists).Returns(mock.Object);
            _userProviderMock.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ListIndexViewModel() { PageNumber = pageNumber });
            var expected = todoLists.Where(x => x.IsHidden != true).OrderBy(x => x.Title).Skip((pageNumber - 1) * 5).Take(5);
            var model = (ListIndexViewViewModel)((ViewResult)result).ViewData.Model;
            var actual = model.List;

            // Assert
            Assert.Equal(expected, actual);
            Assert.False(model.List.HasNextPage);
            Assert.True(model.List.HasPreviousePage);
            Assert.Equal(pageNumber, model.List.PageIndex);
        }

        [Fact]
        public async Task IndexTest_WithPassingHideCompletedLists()
        {
            // Arrange
            bool hideCompletedLists = true;
            var todoLists = GetTodoListList();
            var mock = todoLists.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoLists).Returns(mock.Object);
            _userProviderMock.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ListIndexViewModel() { HideCompletedLists = hideCompletedLists });
            var expected = todoLists.Where(x => x.IsHidden != true).Where(x=> x.TodoListStatus != Status.Completed).OrderBy(x => x.Title);
            var model = (ListIndexViewViewModel)((ViewResult)result).ViewData.Model;
            var actual = model.List;

            // Assert
            Assert.Equal(expected, actual);
            Assert.Equal(hideCompletedLists, model.HideCompletedLists);
        }

        [Fact]
        public async Task IndexTest_WithPassingHideListID_AndReturnsBadRequestObjectResult_WhentTodoListIsNotUpdated()
        {
            // Arrange
            var todoList = GetTodoListList().Where(x => x.IsHidden == false).First();
            _repositoryMock.Setup(x => x.TodoLists).Returns((new TodoList[] { todoList }).AsQueryable());
            _repositoryMock.Setup(x => x.UpdateTodoListAsync(It.IsAny<TodoList>())).ReturnsAsync(false);
            _userProviderMock.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ListIndexViewModel() { HideListID = todoList.ID });

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task IndexTest_WithPassingShowHiddenLists()
        {
            // Arrange
            var todoLists = GetTodoListList(); 
            var mock = todoLists.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoLists).Returns(mock.Object);
            _userProviderMock.Setup(x => x.GetUserId()).Returns("1");

            // Act
            var result = await _controller.Index(new ListIndexViewModel() { ShowHiddenLists = false });
            var expected = todoLists.Where(x => x.IsHidden != true).OrderBy(x => x.Title);
            var model = (ListIndexViewViewModel)((ViewResult)result).ViewData.Model;

            // Assert
            Assert.Equal(expected, model.List);

        }
        #endregion IndexTest

        #region CopyTest

        [Fact]
        public async Task CopyTest_ReturnsNotFoundResult_WhenTodoListIsNotNull()
        {
            // Act
            var result = await _controller.Copy(10, string.Empty);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task CopyTest_ReturnBadRequestObject_WhenTodoListIsNotCreated()
        {
            // Arrange
            var todoLists = GetTodoListList();
            var mock = todoLists.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoLists).Returns(mock.Object);
            _repositoryMock.Setup(x => x.CreateTodoListAsync(It.IsAny<TodoList>())).ReturnsAsync(false);

            // Act
            var result = await _controller.Copy(1, string.Empty);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task CopyTest_ReturnsRedirectResult()
        {
            // Arrange
            var todoLists = GetTodoListList();
            var category = todoLists.FirstOrDefault().Category;
            var mock = todoLists.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoLists).Returns(mock.Object);
            _repositoryMock.Setup(x => x.CreateTodoListAsync(It.IsAny<TodoList>())).ReturnsAsync(true);

            // Act
            var result = await _controller.Copy(1, category);

            // Assert
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal(category, viewResult.RouteValues["category"]);
            Assert.Equal("Index", viewResult.ActionName);
        }

        [Fact]
        public async Task CopyTest_ReturnsRedirectToActionResult()
        {
            // Arrange
            var todoLists = GetTodoListList();
            var mock = todoLists.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoLists).Returns(mock.Object);
            _repositoryMock.Setup(x => x.CreateTodoListAsync(It.IsAny<TodoList>())).ReturnsAsync(true);

            // Act
            var result = await _controller.Copy(1, string.Empty);

            // Assert
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Index", viewResult.ActionName);
        }

        #endregion CopyTest

        #region AddTest

        [Fact]
        public void AddTest_ReturnsViewResult()
        {
            // Action 
            var result = _controller.Add();

            // Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async Task AddTest_ReturnsViewResult_WhenTodoListIsNotValid()
        {
            // Arrange
            _controller.ModelState.AddModelError("Error", "Error");
            var model = GetTodoListList().First();
            // Act
            var result = await _controller.Add(model);

            // Assert
            Assert.IsType<ViewResult>(result);
            Assert.Equal(model, (TodoList)((ViewResult)result).ViewData.Model);
        }

        [Fact]
        public async Task AddTest_ReturnsBadRequestObjectResult_WhenTodoListIsNotCreated()
        {
            // Arrange
            var todoList = GetTodoListList().First();
            _repositoryMock.Setup(x => x.CreateTodoListAsync(It.IsAny<TodoList>())).ReturnsAsync(false);
            _userProviderMock.Setup(x => x.GetUserId()).Returns("userId");

            // Act
            var result = await _controller.Add(todoList);

            // Assert
            var viewResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Could not create new todo list", viewResult.Value.ToString());
        }

        [Fact]
        public async Task AddTest_AddsTodoListToRepository_And_ReturnsRedirectToActionResult()
        {
            // Arrange
            var todoList = GetTodoListList().First();
            _repositoryMock.Setup(x => x.CreateTodoListAsync(It.IsAny<TodoList>())).ReturnsAsync(true);
            _userProviderMock.Setup(x => x.GetUserId()).Returns("userId");

            // Act
            var result = await _controller.Add(todoList);

            // Assert
            _repositoryMock.Verify(x => x.CreateTodoListAsync(todoList), Times.Once);
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Index", viewResult.ActionName);
        }

        [Fact]
        public async Task AddTest_SetsUserId_BeforeAddingArticleToRepository()
        {
            // Arrange
            string userId = "userId";
            var todoList = GetTodoListList().First();
            _userProviderMock.Setup(x => x.GetUserId()).Returns(userId);

            // Act
            var result = await _controller.Add(todoList);

            // Assert
            _repositoryMock.Verify(x => x.CreateTodoListAsync(It.Is<TodoList>(x => x == todoList
                && x.ApplicationUserId == userId)), Times.Once);
        }

        #endregion AddTest

        #region DeleteTest

        [Fact]
        public async Task DeteteTest_ReturnsNotFound_WhenIdIsNull()
        {
            // Act
            var result = await _controller.Delete(null, null);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task DeleteTest_ReturnsBadRequestObjectResult_WhenTodoListIsNotDeleted()
        {
            // Arrange
            var todoList = GetTodoListList().First();
            _repositoryMock.Setup(x => x.DeleteTodoListAsync(It.IsAny<int>())).ReturnsAsync(false);

            // Act
            var result = await _controller.Delete(todoList.ID, string.Empty);

            // Assert
            var viewResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Could not remove todo list", viewResult.Value.ToString());
        }

        [Fact]
        public async Task DeleteTest_ReturnsRedirectToActionResult_WhenReturnUrlIsNullOrEmpty()
        {
            // Arrange
            var todoList = GetTodoListList().First();
            _repositoryMock.Setup(x => x.DeleteTodoListAsync(It.IsAny<int>())).ReturnsAsync(true);
            //_repositoryMock.Setup(x => x.TodoLists).Returns((new TodoListWeb[] { todoList }).AsQueryable<TodoListWeb>());
            //_repositoryMock.Setup(x => x.CreateTodoList(todoList));
            //_userProviderMock.Setup(x => x.GetUserId()).Returns(userId);

            // Act
            var result = await _controller.Delete(todoList.ID, string.Empty);
            // var expected = _repositoryMock.Object.TodoLists.FirstOrDefault();

            // Assert
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Index", viewResult.ActionName);
        }

        [Fact]
        public async Task DeleteTest_DeleteTodoList_AndReturnsRedirectResult_WhenReturnUrlIsNotNullOrEmpty()
        {
            // Arrange
            var todoLists = GetTodoListList();
            var todoList = todoLists.First();
            var category = todoList.Category;
            _repositoryMock.Setup(x => x.DeleteTodoListAsync(It.IsAny<int>())).ReturnsAsync(true);
            var mock = todoLists.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoLists).Returns(mock.Object);
            //_repositoryMock.Setup(x => x.TodoLists).Returns((new TodoListWeb[] { todoList }).AsQueryable<TodoListWeb>());
            //_repositoryMock.Setup(x => x.CreateTodoList(todoList));
            //_userProviderMock.Setup(x => x.GetUserId()).Returns(userId);

            // Act
            var result = await _controller.Delete(todoList.ID, category);
            // var expected = _repositoryMock.Object.TodoLists.FirstOrDefault();

            // Assert
            _repositoryMock.Verify(x => x.DeleteTodoListAsync(todoList.ID), Times.Once);
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal(category, viewResult.RouteValues["category"]);
            Assert.Equal("Index", viewResult.ActionName);
        }

        #endregion DeleteTest

        #region UpdateTest

        [Fact]
        public void UpdateTest_ReturnsNotFound_WhenIdIsNull()
        {
            // Act
            var result = _controller.Update(id: null, string.Empty);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void UpdateTest_ReturnsNotFound_WhenTodoListIsNotExist()
        {
            // Act
            var result = _controller.Update(id: 1, string.Empty);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void UpdateTest_ReturnsViewResult_WhenTodoListIsExist()
        {
            // Arrange
            var returnUrl = "return.Url";
            var todoLists = GetTodoListList();
            var mock = todoLists.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoLists).Returns(mock.Object);

            // Act
            var result = _controller.Update(todoLists.First().ID, returnUrl);
            var expected = _mapper.Map<ListUpdateViewModel>(todoLists.First());
            expected.ReturnUrl = returnUrl;

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Equal(expected, (ListUpdateViewModel)((ViewResult)result).ViewData.Model);
        }

        [Fact]
        public async Task UpdateTest_ReturnsViewResult_WhenTodoLisIsNotValid()
        {
            // Arrange
            _controller.ModelState.AddModelError("Error", "Error");
            var viewModel = GetListUpdateViewModel();

            // Act
            var result = await _controller.Update(viewModel);

            // Assert
            Assert.IsType<ViewResult>(result);
            Assert.Equal(viewModel, (ListUpdateViewModel)((ViewResult)result).ViewData.Model);
        }

        [Fact]
        public async Task UpdateTest_ReturnsBadRequestObjectResult_WhenTodoListIsNotUpdated()
        {
            // Arrange
            var viewModel = GetListUpdateViewModel();
            _userProviderMock.Setup(x => x.GetUserId()).Returns("userId");
            _repositoryMock.Setup(x => x.UpdateTodoListAsync(It.IsAny<TodoList>())).ReturnsAsync(false);
            // Act

            var result = await _controller.Update(viewModel);

            // Assert
            var viewResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Could not update todo list", viewResult.Value.ToString());
        }

        [Fact]
        public async Task UpdateTest_ReturnsRedirectResult_WhenReturnUrlIsNotNullOrEmpty()
        {
            // Arrange
            var viewModel = GetListUpdateViewModel();
            _userProviderMock.Setup(x => x.GetUserId()).Returns("userId");
            _repositoryMock.Setup(x => x.UpdateTodoListAsync(It.IsAny<TodoList>())).ReturnsAsync(true);
            var todoLists = GetTodoListList();
            var mock = todoLists.AsQueryable().BuildMock();
            _repositoryMock.Setup(x => x.TodoLists).Returns(mock.Object);

            // Act
            var result = await _controller.Update(viewModel);

            // Assert
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal(viewModel.ReturnUrl, viewResult.RouteValues["category"]);
            Assert.Equal("Index", viewResult.ActionName);
        }


        [Fact] 
        public async Task UpdateTest_UpdateTodoList_And_ReturnsRedirectToActionResult_WhenReturnUrlIsNullOrEmpty()
        {
            // Arrange
            string userId = "userId";
            var viewModel = GetListUpdateViewModel();
            viewModel.ReturnUrl = null;
            _userProviderMock.Setup(x => x.GetUserId()).Returns(userId);
            _repositoryMock.Setup(x => x.UpdateTodoListAsync(It.IsAny<TodoList>())).ReturnsAsync(true);
            // Act

            var result = await _controller.Update(viewModel);

            // Assert
            var viewResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Index", viewResult.ActionName);
        }

        [Fact]
        public async Task UpdateTest_UpdateTodoList_And_SetUserIdBeforeUpdateTodoList()
        {
            // Arrange
            string userId = "userId";
            var viewModel = GetListUpdateViewModel();
            _userProviderMock.Setup(x => x.GetUserId()).Returns(userId);

            // Act
            await _controller.Update(viewModel);

            // Assert
            _repositoryMock.Verify(x => x.UpdateTodoListAsync(It.Is<TodoList>(x => x.ApplicationUserId == userId)), Times.Once);
        }

        #endregion UpdateTest

        public List<TodoList> GetTodoListList()
        {
            var todoLists = new List<TodoList>()
            {
                 new TodoList { ID = 1, Category = "Sport", Title = "Soccer",  ApplicationUserId = "1", IsHidden = true, TodoListStatus = Status.Completed },
                 new TodoList { ID = 2, Category = "Home",  Title = "TV",  ApplicationUserId = "1", IsHidden = false, TodoListStatus = Status.NotStarted },
                 new TodoList { ID = 3, Category = "Job", Title = "Sport article",  ApplicationUserId = "1", IsHidden = true, TodoListStatus = Status.InProgress },
                 new TodoList { ID = 4, Category = "Sport", Title = "Football",  ApplicationUserId = "1", IsHidden = false, TodoListStatus = Status.Completed },
                 new TodoList { ID = 5, Category = "Home",  Title = "Food",  ApplicationUserId = "1", IsHidden = true, TodoListStatus = Status.NotStarted },
                 new TodoList { ID = 6, Category = "Job", Title = "Reports",  ApplicationUserId = "1", IsHidden = false, TodoListStatus = Status.InProgress },
                 new TodoList { ID = 7, Category = "Home", Title = "Sleep",  ApplicationUserId = "1", IsHidden = true, TodoListStatus = Status.Completed },
            };

            return todoLists;
        }

        private ListUpdateViewModel GetListUpdateViewModel()
        {
            return new ListUpdateViewModel()
            {
                ReturnUrl = "Sport",
                ID = 1,
                Category = "Category",
                TodoListStatus = Status.NotStarted,
                TodoListPriority = Priority.Low,
                ApplicationUserId = "userID",
                Title = "Title"
            };
        }

    }
}
